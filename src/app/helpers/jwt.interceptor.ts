import { Injectable, Inject } from '@angular/core';
import {
  HttpRequest, HttpHandler, HttpInterceptor, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse,
  HttpUserEvent, HttpErrorResponse
} from '@angular/common/http';
import { Observable, BehaviorSubject, throwError, } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { catchError, switchMap, finalize, filter, take } from 'rxjs/operators';
import { UserInfo } from '../models/UserInfo.model';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  baseUrl = 'http://localhost:50186';
  isRefreshingToken = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  constructor(private authService: AuthenticationService) { }
  private addTokenToRequest(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({ setHeaders: { Authorization: `Bearer ${token}` } });
  }
  intercept(request: HttpRequest<any>, next: HttpHandler):
    Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {
    return next.handle(this.addTokenToRequest(request, this.authService.getToken()))
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse && request.url.indexOf('/Auth/login') === -1) {
            switch ((err as HttpErrorResponse).status) {
              case 401:
                if (err.url.indexOf(this.baseUrl) >= 1) {
                  return this.handle401Error(request, next);
                }
              case 400:
                // return <any>this.authService.logout();
                return throwError(err);
              case 0:
                return throwError(err);
            }
          } else {
            return throwError(err);
          }
        }));
  }
  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    this.isRefreshingToken = false;
    return this.tokenSubject
      .pipe(filter(token => token != null),
        take(1),
        switchMap(token => {
          return next.handle(this.addTokenToRequest(request, token));
        }));
  }

}
