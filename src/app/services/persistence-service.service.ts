import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class PersistenceServiceService {

  constructor() {


  }
/**
 * guardar datos en el localstorage
 * @param type nombre del la variable
 * @param data dato a guardar
 * @param isJson valida si esta en formato json si no lo combierte
 */
  setData(type: string, data: any, isJson: boolean = false) {
    localStorage.setItem(type, isJson ? JSON.stringify(data) : data);
  }
/**
 * obtiene datos del localstorage
 * @param type variable
 * @param isJson valida si esta en formato json si no lo combierte
 */
  getData(type: string, isJson: boolean = false): any {
    try {
      const data = localStorage.getItem(type);
      return isJson ? data != undefined && data != null ? JSON.parse(data) : undefined : data;
    } catch (error) {
      return null;
    }
  }
/**
 * limpiar localstorage
 */
  removeData(type: string) {
    localStorage.removeItem(type);
  }
}
