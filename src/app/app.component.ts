import { Component, ViewChild, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { MediaObserver } from '@angular/flex-layout';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { PersistenceServiceService } from 'src/app/services/persistence-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  title = 'TiendaCliente';
  isLogin = false;
  name = '';
  isExpanded = true;
 showMenu = false;
  sub: Subscription;
  opened = true;
  public config: PerfectScrollbarConfigInterface = {};
  @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;
  constructor(public media: MediaObserver,
              private authenticationService: AuthenticationService,
              private router: Router,
              private persistenceService: PersistenceServiceService) {
    this.isLogin = authenticationService.IsLogin();
    if (this.isLogin) {
      this.name = authenticationService.getName();
    }
  }
  logout() {
    this.authenticationService.logout();
    this.isLogin = false;
  }
}
