import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AuthGuard } from './services/guard.service';
import { LoginComponent } from './pages/login/login.component';
import { ClientsComponent } from './pages/clients/clients.component';
import { AddClientComponent } from './pages/add-client/add-client.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { UpdateClientComponent } from './pages/update-client/update-client.component';

const routes: Routes = [

  { path: 'inicio', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'clientes', component: ClientsComponent, canActivate: [AuthGuard] },
  { path: 'Agregar-Cliente', component: AddClientComponent, canActivate: [AuthGuard] },
  { path: 'Actualizar-Cliente/:id', component: UpdateClientComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: 'inicio', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
