import { Injectable } from '@angular/core';
import { UserInfo } from '../models/UserInfo.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { RequestResult } from 'src/app/models/RequestResult.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { PersistenceServiceService } from 'src/app/services/persistence-service.service';
import { Clientes } from '../models/Clientes.Model';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  baseUrl = 'http://localhost:50186';
  error: HttpErrorResponse;
  constructor(private router: Router,
              private http: HttpClient, private persistenceService: PersistenceServiceService) { }

/**
 * Listado de todos los usaurios
 */
  async ListadoCLientes() {
    // const result = await this.http.get<Clientes[]>(`${this.baseUrl}/api/clientes/Listado`).toPromise();
    const result = await this.http.get<Clientes[]>(`${this.baseUrl}/api/clientes/ListadoSp`).toPromise();
    console.log(result);
    return result;
  }

  /**
   * Eliminar cliente por su id
   */
  async EliminarCliente(id: number): Promise<RequestResult> {
    // const result = await this.http.delete<RequestResult>(`${this.baseUrl}/api/clientes/${id}`).toPromise();
    const result = await this.http.delete<RequestResult>(`${this.baseUrl}/api/clientes/EliminarSp/${id}`).toPromise();
    console.log(result);
    return result;
  }
  /**
   * Crea un nuevo cliente
   * @param cliente Datos del cliente a crear
   */
  async CrearCliente(cliente: Clientes): Promise<Clientes> {

    // const result = await this.http.post<Clientes>(`${this.baseUrl}/api/clientes`, cliente).toPromise();
    const result = await this.http.post<Clientes>(`${this.baseUrl}/api/clientes/InsertarClienteSp`, cliente).toPromise();
    return result;
  }
  /**
   * Consulta un cliente por su id
   * @param id id del cliente a buscar
   */
  async BuscarClientePorId(id: any): Promise<Clientes> {
    // const result = await this.http.get<Clientes>(`${this.baseUrl}/api/clientes/${id}`).toPromise();
    const result = await this.http.get<Clientes>(`${this.baseUrl}/api/clientes/ObtenerClienteSp/${id}`).toPromise();
    return result;

  }
  /**
   * Actualiza un cliente
   * @param cliente Datos del cliente a actualizar
   */
  async ActualizarCliente(cliente: Clientes, id) {
    cliente.idCliente = id;
    const result = await this.http.put<Clientes>(`${this.baseUrl}/api/clientes/Actualizar`, cliente).toPromise();
    // const result = await this.http.put<Clientes>(`${this.baseUrl}/api/clientes/ActualizarSp`, cliente).toPromise();
    return result;
  }
}
