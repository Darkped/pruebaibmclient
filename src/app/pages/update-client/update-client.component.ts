import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClientsService } from 'src/app/services/clients.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { Clientes } from 'src/app/models/Clientes.Model';

@Component({
  selector: 'app-update-client',
  templateUrl: './update-client.component.html',
  styleUrls: ['./update-client.component.scss']
})
export class UpdateClientComponent implements OnInit {
  FormClientes: FormGroup;
  id: any;
  editarActivo = false;
  cliente: Clientes = new Clientes();
  constructor(
    private clienteServices: ClientsService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private location: Location
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    if (this.id) {
      this.CargarDatosCliente();
    }

    this.FormClientes = new FormGroup({
      primerNombre: new FormControl('', [
        Validators.required,
      ]),
      segundoNombre: new FormControl(),
      primerApellido: new FormControl('', [
        Validators.required,
      ]),
      segundoApellido: new FormControl(),
      telefono: new FormControl('', [
        Validators.required,
        Validators.pattern(/^\d{1,250}$/)
      ]),
      direccion: new FormControl('', [
        Validators.required,
      ])
    });
  }

  goback() {
    this.location.back();
  }

  async onSubmit() {
    console.log(this.FormClientes);

    if (!this.FormClientes.pristine) {
      const actualizado = await this.clienteServices.ActualizarCliente(this.FormClientes.value, this.id);
      if (actualizado) {
        this.router.navigate(['/clientes']);
      }
    } else {
      this.openSnackBar('No se ha realizado ninguna modificación');
    }
  }
  limpiarForm() {
    this.FormClientes.reset();
  }
  openSnackBar(message: string, action: string = '', duration: number = 2000) {
    this.snackBar.open(message, action, {
      duration,
    });
  }
  editarDatos() {
    this.FormClientes.enable();
    this.editarActivo = true;
  }
  async CargarDatosCliente() {
    this.cliente = new Clientes();
    this.cliente = await this.clienteServices.BuscarClientePorId(this.id);
    this.FormClientes.patchValue(this.cliente);
    this.FormClientes.disable();
  }

}
