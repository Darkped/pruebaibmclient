export class UserInfo {
  idSesion: number;
  name: string;
  token: string;
  expiration: any;
}
