import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClientsService } from 'src/app/services/clients.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {
  FormClientes: FormGroup;
  isSubmit = false
  constructor(
    private clienteServices: ClientsService,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.FormClientes = new FormGroup({
      primerNombre: new FormControl('', [
        Validators.required,
      ]),
      segundoNombre: new FormControl(),
      primerApellido: new FormControl('', [
        Validators.required,
      ]),
      segundoApellido: new FormControl(),
      telefono: new FormControl('', [
        Validators.required,
        Validators.pattern(/^\d{1,250}$/)
      ]),
      direccion: new FormControl('', [
        Validators.required,
      ])
    });
  }
  /**
   * Guardar cliente
   */
  async onSubmit() {
    this.isSubmit = true;
    // console.log(this.FormClientes);

    if(this.FormClientes.valid){
      const nuevo = await this.clienteServices.CrearCliente(this.FormClientes.value);
      if(nuevo){
        this.router.navigate(['/clientes']);
      }
    }
  }
  /**
   * limpiar campos
   */
  limpiarForm() {
    this.FormClientes.reset();
  }
  /**
   * Regresar atras
   */
  goback() {
    this.location.back();
  }

}
