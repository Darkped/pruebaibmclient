import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild, Pipe, PipeTransform, ElementRef, TemplateRef, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { MediaObserver } from '@angular/flex-layout';
import { Clientes } from 'src/app/models/Clientes.Model';
import { ClientsService } from 'src/app/services/clients.service';
@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {
  ListaClientes: Clientes[] = [];
  dialogRef: MatDialogRef<any, any>;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild('DeleteConfirmation', { static: false }) private DeleteConfirmationRef: TemplateRef<any>;
  idCliente: any;
  isDeleting = false;
  isLoadingResults = false;
  isRateLimitReached = false;
  constructor(
    private router: Router,
    public media: MediaObserver,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private clientesServices: ClientsService,
    private route: ActivatedRoute
  ) { }
  displayedColumns: string[] = ['nombres', 'apellidos', 'telefono', 'direccion', 'acciones'];
  dataSource = new MatTableDataSource();

  /**
   * filtro tabla
   * @param filterValue Valor a filtrar
   */
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngOnInit() {
    this.ListaClientes = [];
    this.ObtenerClientes();

  }
  /**
   * Envia a pagina de actualizar cliente
   * @param item cliente selecionado
   */
  irActualizar(item) {
    this.router.navigate([`/Actualizar-Cliente/${item.idCliente}`]);
  }

  openSnackBar(message: string, action: string = '', duration: number = 2000) {
    this.snackBar.open(message, action, {
      duration,
    });
  }
  /**
   * Ventana emergente para eliminar cliente
   * @param item cliente selecionado
   */
  dialogEliminarCliente(item) {
    this.idCliente = item.idCliente;
    this.dialogRef = this.dialog.open(this.DeleteConfirmationRef, {
      width: '350px',
      data: item.id,
      disableClose: true
    });
  }
  /**
   * Eliminar cliente
   */
  async EliminarCliente() {
    this.isDeleting = true;
    const resultado = await this.clientesServices.EliminarCliente(this.idCliente);
    this.isDeleting = false;
    this.cerrarDiag();
    this.ObtenerClientes();
    this.openSnackBar(resultado.mensaje);
  }
  /**
   * cerrar ventana emergentes
   */
  cerrarDiag() {
    this.dialogRef.close();
  }
  /**
   * lista todos los clientes
   */
  async ObtenerClientes() {
    try {
      this.isLoadingResults = true;
      this.ListaClientes = await this.clientesServices.ListadoCLientes();
      if (this.ListaClientes == null) {
        this.isRateLimitReached = true;
        this.isLoadingResults = false;
      } else {
        this.dataSource = new MatTableDataSource(this.ListaClientes);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.isLoadingResults = false;
      }

    } catch (error) {
      console.log(error);
      this.isRateLimitReached = true;
      this.isLoadingResults = false;
    }



  }
}
