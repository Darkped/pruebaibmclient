import { Injectable } from '@angular/core';
import { UserInfo } from '../models/UserInfo.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { RequestResult } from 'src/app/models/RequestResult.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { PersistenceServiceService } from 'src/app/services/persistence-service.service';
import { Usuarios } from '../models/Usuarios.Model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  baseUrl = 'http://localhost:50186';
  error: HttpErrorResponse;
  constructor(private router: Router,
              private http: HttpClient, private persistenceService: PersistenceServiceService) { }

  /**
   * Cerrar sesion
   */
  logout() {
    this.persistenceService.removeData('currentUser');
    document.location.href = '/';

  }
  /**
   * Verifica si esta logueado
   */
  IsLogin(): boolean {
    const exp = this.getExpirationToken();
    if (!exp) {
      return false;
    }
    const now = new Date().getTime();
    const dateExp = new Date(exp);

    if (now >= dateExp.getTime()) {
      // ya expiró el token
      this.logout();
      return false;
    } else {
      return true;
    }
  }
  /**
   * Login usuario
   * @param username usuario
   * @param password Contraseña
   */
  async login(username: string, password: string) {
    const user = await this.http.post<UserInfo>(`${this.baseUrl}/api/Auth/login`,
      {
        username,
        password
      }).toPromise();

    if (user && user.token) {
      this.persistenceService.setData('currentUser', user, true);
    }
    return user;
  }
  /**
   * Registrar un nuevo usuario
   */
  async registro(usuario: Usuarios) {
    const request = await this.http.post<RequestResult>(`${this.baseUrl}/api/auth/Crear`, usuario).toPromise();
    return request;
  }
  /**
   * Obtiene el usaurio actual logueado
   */
  getCurrentUser() {
    return this.persistenceService.getData('currentUser', true);
  }
  /**
   * Obtiene el nombre del usaurio actual logueado
   */
  getName(): string {
    const json = this.getCurrentUser();
    return json == null ? '' : json.nombre;
  }

  /**
   * Obtiene el id del usuario actual logueado
   */
  getUserId(): number {
    const json = this.getCurrentUser();
    return json == null ? '' : json.id;
  }
  /**
   * Obtiene la expiracion del token actual
   */
  getExpirationToken(): string {
    const json = this.getCurrentUser();
    return json == null ? null : json.expiration;
  }
/**
 * Obtiene el token actual
 */
  getToken(): string {
    const json = this.getCurrentUser();
    return json == null ? '' : json.token;
  }
}
