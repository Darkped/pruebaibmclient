import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MatDialogRef, MatSnackBar, MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  loading = false;
  messageLoading = '';
  isLinear = false;
  FormUsuarios: FormGroup;
  isSubmitRegister = false;
  selectedIndex: number;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authenticationService: AuthenticationService,
              private snackBar: MatSnackBar

  ) { }
  async ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.FormUsuarios = new FormGroup({
      nombre: new FormControl('', [
        Validators.required,
      ]),
      apellido: new FormControl('', [
        Validators.required,
      ]),
      usuario: new FormControl('', [
        Validators.required,
      ]),
      password: new FormControl('', [
        Validators.required
      ]),
    });
  }
  async onSubmitUsuarios() {
    if (this.FormUsuarios.invalid) {
      return;
    }
    this.loading = true;
    try {

      this.messageLoading = 'Registrando usuario...';
      const dataLogin = await this.authenticationService.registro(this.FormUsuarios.value);
      this.openSnackBar(dataLogin.mensaje);
      this.loading = false;
      if (dataLogin.registros === 1) {
        this.selectedIndex = 0;
        this.FormUsuarios.reset();
      }

    } catch (HttpErrorResponse) {
      this.messageLoading = 'error al crear el usaurio';
    }

  }
  openSnackBar(message: string, action: string = '', duration: number = 2000) {
    this.snackBar.open(message, action, {
      duration,
    });
  }
  async onSubmit() {
    this.submitted = true;
    console.log(this.loginForm);
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    try {

      this.messageLoading = 'Validando usuario';
      const dataLogin = await this.authenticationService.login(this.loginForm.controls.username.value,
        this.loginForm.controls.password.value);
      if (dataLogin) {
        this.messageLoading = 'Cargando pagina principal';
        document.location.href = '/inicio';
        this.loading = false;
      }

    } catch (HttpErrorResponse) {
      this.messageLoading = 'error';
    }
  }



}
